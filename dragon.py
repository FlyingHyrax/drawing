from turtle import *
from copy import copy

from collections import deque
from itertools import islice, cycle
from colorsys import hsv_to_rgb

# cardinal directions
UP = "up"
DOWN = "down"
LEFT = "left"
RIGHT = "right"

# map each direction to a heading in degree in the turtle coordinate system
degree_map = {
	UP: 90,
	DOWN: 270,
	LEFT: 180,
	RIGHT: 0,
}


# translate our heading constants to headings in the standard turtle coordinate system
def heading_to_degrees(heading):
	return degree_map[heading]

# function factory:
def make_move(heading, dist=5):
	def move(pen):
		pen.setheading(heading_to_degrees(heading))
		pen.forward(dist)
	return move

pen = Turtle()

screen = pen.getscreen()
screen.mode("standard")
screen.colormode(1.0)
screen.delay(2)
screen.tracer(2)

pen.hideturtle()
pen.pen(shown=False, pendown=True, pencolor="black", pensize=2)


# map each direction to its adjacent direction turning clockwise
cw_neighbor_map = {
	UP: RIGHT,
	RIGHT: DOWN,
	DOWN: LEFT,
	LEFT: UP,
}

# map each direction to its adjacent neighbor turning counterclockwise
ccw_neighbor_map = {
	UP: LEFT,
	RIGHT: UP,
	DOWN: RIGHT,
	LEFT: DOWN,
}

# rotations: given a cardinal direction, return new cardinal direction
# from rotating 90 degrees either clockwise or counterclockwise:
def rotate_cw(heading):
	return cw_neighbor_map[heading]

def rotate_ccw(heading):
	return ccw_neighbor_map[heading]

# pick a direction to rotate:
rotate = rotate_ccw

def dragon0(degree, initial=[RIGHT, UP]):
    result = initial
    for _ in range(degree):
        result = result + [rotate(d) for d in result[::-1]]
    return result

def dragon1():
    new = deque([RIGHT, UP])
    used = deque()
    while True:
        if len(new) == 0:
            # generate more
            for d in used:
                new.appendleft(rotate(d))

        to_yeild = new.popleft()
        yield to_yeild
        used.append(to_yeild)

def hues(sat=0.8, val=0.5):
    for h in range(1001):
        yield hsv_to_rgb((h / 1000), sat, val)


move_seq = map(make_move, islice(dragon1(), 2048))
color_seq = cycle(hues())

for (move, color) in zip(move_seq, color_seq):
    pen.pencolor(color)
    move(pen)

#screen.exitonclick()
screen.mainloop()
